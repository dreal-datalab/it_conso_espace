
## Les sources

### Occupation du sol à grande échelle (OCS GE)

L’OCS GE est une base de données vectorielle pour la description de l’occupation du sol de l’ensemble du territoire métropolitain et des départements et régions d’outre-mer (DROM). Cependant, toutes les régions ne sont pas encore couvertes.

La région des Pays de la Loire dispose de deux millésimes : 2013 et 2016 au 30 juin de l'année.

L'OCSGE s’appuie sur un modèle ouvert séparant la couverture du sol et l’usage du sol (appelé modèle en 2 dimensions), une précision géométrique appuyée sur le Référentiel à Grande Échelle (RGE®) et une cohérence temporelle (notion de
millésime) qui, par le biais de mises à jour successives, permet de quantifier et de qualifier les évolutions des
espaces.

L’exploitation de la source s’est appuyé sur les travaux de la DDTM de Vendée en 2017 pour définir une typologie des espaces
à partir du croisement des valeurs attributaires de couverture (CS) et d’usage du sol (US): espace artificialisé, espace agricole, surface naturelle boisée, autre surface naturelle, surface en eau.

#### Pour les espaces artificialisés :

- l’ensemble des surfaces anthropisées (CS 1.1) pour tous les usages du sol ;

- les sols nus (CS 1.2.1) croisés avec l’ensemble des usages du sol sauf la sylviculture (US 1.2), les productions
primaires autres (US 1.5), les zones sans usage (US 6.3) ou à l’usage inconnu (US 6.4) ;

- les sols avec végétation ligneuse (CS 2.1) croisés avec usage d’extraction (US 1.3) et usage de production secondaire, tertiaire et usage résidentiel (US 235) ;

- les formations herbacées (CS 2.2.1) hors usage d’agriculture (US 1.1), de sylviculture (US 1.2), sans usage (US 6.3) ou usage inconnu (US 6.4).

#### Pour les espaces agricoles :

- les sols avec végétation ligneuse (CS 2.1) et les formations herbacées (CS 2.2.1) croisés avec usage d’agriculture (US 1.1).

#### Pour les surfaces naturelles boisées :

- les sols avec végétation ligneuse (CS 2.1) croisés avec usage de sylviculture (US 1.2), de pêche et aquaculture (US 1.4) et autre (US 1.5), de réseaux de transport, logistiques, infrastructures (US 4) et autre usage (uS 6).

#### Pour les autres surfaces naturelles :

- les sols nus (CS 1.2.1) croisés avec usage de sylviculture (US 1.2) et autre (US 1.5), sans usage (US 6.3) et usage inconnu (US 6.4) ;

- les formations herbacées (CS 2.2.1) croisées avec usage de sylviculture (US 1.2), sans usage (US 6.3) et usage inconnu (US 6.4).

#### Pour les surfaces en eau :

- les surfaces en eau (CS 1.2.2) pour tous les usages du sol.

### Observatoire de l'artificialisation

L’artificialisation se définit communément comme la transformation d’un sol naturel, agricole ou forestier, par des opérations d’aménagement pouvant entraîner une imperméabilisation partielle ou totale, afin de les affecter notamment à des fonctions urbaines ou de transport (habitat, activités, commerces, infrastructures, équipements publics…).

Les flux d’artificialisation annuels constituent le cœur des données produites. Il s’agit de l’artificialisation calculée à partir des fichiers fonciers, avec la définition décrite dans le [rapport disponible en ligne](https://artificialisation.biodiversitetousvivants.fr/sites/artificialisation/files/inline-files/artificialisation%20rapport%20final%20V3.pdf). 

Les données sont disponibles pour les années 2010 à 2019 au 1er janvier.

### Geokit3

GéoKit3225 est un Infocentre, dont le but est de diffuser auprès des agents du ministère de la transformation écologique des données communales, avec un accès aux données confidentielles pour les statisticiens. 

En l'occurrence, l’outil intègre des données statistiques sur les stocks des espaces artificialisés et des espaces naturels, agricoles ou forestiers au 1er janvier 2017, calculés à partir des fichiers fonciers. 

A partir des flux d'artificialisation de l'observatoire de l'artificialisation et des stocks de l'année 2017 de Geokit3, il est possible de reconstituer les stocks d'années antérieures ou postérieures.

L'indicateur d'étalement urbain met en regard le taux de croissance de la population et le taux de croissance de l'artificialisation. Ce dernier nécessite de disposer des stocks des deux années qui déterminent la période, en l'occurrence 2010 et 2018. 

### Plan cadastral informatisé (PCI)

Le plan cadastral est un assemblage de feuilles ou planches représentant chacune une section ou une partie d’une section cadastrale. Il couvre la France entière, à l’exception de la ville de Strasbourg et de quelques communes voisines, pour des raisons historiques liées à l’occupation de l’Alsace-Moselle par l’Allemagne entre 1871 et 1918.

Le plan cadastral est géré par la Direction Générale des Finances Publiques (DGFiP).

Les données sont disponibles pour les années 2011 à 2018 au 1er janvier.

### Population légale de l'INSEE

L'INSEE met à disposition sur [son site](https://www.insee.fr/fr/statistiques/2522602#consulter) l'historique des populations légales des communes de France métropolitaine aux recensements de la population de 1968, 1975, 1982, 1990, 1999, et de 2006 à 2018 au 1er janvier. Pour les communes des départements d'outre-mer (hors Mayotte), de Saint-Barthélemy, de Saint-Martin et de Saint-Pierre-et-Miquelon, elles ne sont disponibles qu'à partir de 1990.

Jusqu'en 1999, les populations légales étaient déterminées à l'occasion de chaque recensement général de la population.

Depuis 2008, la nouvelle méthode de recensement basée sur des enquêtes de recensement annuelles permet de calculer chaque année des populations légales actualisées.

### Logements et résidences principales (Recensement de la population)

L'INSEE met à disposition sur [son site](https://www.insee.fr/fr/statistiques/4515532?sommaire=4516107#consulter) des résultats sur les logements, résidences principales, résidences secondaires et logements occasionnels, logements vacants, maisons, appartements. Le bases de données sont fondées sur les résultats du recensement de la population pour les années 2007, 2012, et 2017 au 31 décembre.

## Les indicateurs

### Pourcentage de l'espace artificialisé et typologie de l'occupation des sols au 30 juin 2016

Sources : OCS GE

Datavisualisations : chiffres clés, carte et diagramme barre

Limite : Les données des communes hors région des EPCI inter-régionaux ne sont pas présentes, les totaux de ces EPCI sont donc partiels.

### Surface bâtie par habitant au 1er janvier 2018

Sources : PCI et Population légale de l’INSEE

Datavisualisation : chiffres clés

Limite : Les données PCI des communes hors région des EPCI inter-régionaux ne sont pas présentes, les totaux de ces EPCI sont donc partiels.

### Surface bâtie par logement au 1er janvier 2018

Sources : PCI et Logements et résidences principales

Datavisualisation : chiffres clés

Limite : Les données PCI des communes hors région des EPCI inter-régionaux ne sont pas présentes, les totaux de ces EPCI sont donc partiels.

### Surface artificialisée en 5 ans au 1er janvier 2018

Source : Observatoire de l'artificialisation

Datavisualisations : chiffres clés, carte

### Evolution de la consommation d'espaces naturels, agricoles ou forestiers au 1e janvier 2018

Source : Observatoire de l'artificialisation

Datavisualisation : diagrammes ligne (territoire sélectionné et territoire de comparaison)

### Surface artificialisée par habitant accueilli sur 5 ans au 1er janvier 2018

Sources : Observatoire de l’artificialisation des sols et Population légale de l’INSEE

Datavisualisation : carte

Méthodologie : L’observatoire fournit l'évolution des surfaces artificialisées de 2013 à 2018. Ces évolutions sont sommées et rapportées à la population accueillie par le territoire sur la même période. En cas de décroissance de la population, l’indicateur ne peut être calculé.

### Evolutions comparées entre population et population sur 5 ans au 1er janvier 2018

Sources : Observatoire de l’artificialisation des sols et Population légale de l’INSEE

Datavisualisation : diagramme point

### Etalement urbain entre 2010 et 2018 au 1er janvier 2018

Sources : Observatoire de l’artificialisation des sols, Geokit3 et Population légale de l’INSEE

Datavisualisation : carte




