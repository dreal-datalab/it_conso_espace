<p style="text-align:center";><img src="www/Logo_datalab.jpg" width="20%" ></p>
<br>

<p style="text-align:center";>
Direction régionale de l’environnement, de l’aménagement et du logement des Pays de la Loire</p>

<p style="text-align:center";>Service connaissance des territoires et évaluation</p>

<p style="text-align:center";>5, rue Françoise Giroud - CS16326 - 44263 NANTES Cedex 2 - Tél. 02 72 74 73 00</p>

<p style="text-align:center";>Directrice de la publication : Annick Bonneville</p>

<p style="text-align:center";>ISSN : 2109-0025</p>

<p style="text-align:center";>© DREAL 2020</p>